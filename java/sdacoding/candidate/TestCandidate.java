package sdacoding.candidate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import sdacoding.company.Company;
import sdacoding.department.DepartmentName;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class TestCandidate {
    public static void main(String[] args) {

        Company company = new Company();

        List<Candidate> candidates = null;
        try {
            candidates = readFromJsonFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            candidates = readFromJsonFile();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        company.setCandidates(candidates);
        System.out.println(candidates);
        company.recruiting();

        try {
            writeToFile(candidates);
        } catch (IOException e) {
            System.out.println("Could not write to file");
        }
        try {
            writeToFileJson(candidates);
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println(candidates);
    }

    public static void writeToFile(List<Candidate> candidates)
            throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter("candidate.txt"));
        writer.write(candidates.toString());
        writer.close();


    }

    public static void writeToFileJson(List<Candidate> candidates) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.writeValue(new File("list.json"), candidates);
    }

    public static List<Candidate> readFromJsonFile() throws IOException {

        String content = new String(Files.readAllBytes(Paths.get("candidate.json")));
        ObjectMapper objectMapper = new ObjectMapper();
        List<Candidate> listCandidate = objectMapper.readValue(content, new TypeReference<List<Candidate>>() {
        });
        return listCandidate;
    }
}
