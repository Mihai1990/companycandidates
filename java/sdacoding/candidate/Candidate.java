package sdacoding.candidate;


import sdacoding.department.DepartmentName;

public class Candidate {
    private String name;
    private int levelOfCompetence;
    private DepartmentName departmentName;
    private CandidateStatus status;

    public Candidate() {
    }

    public Candidate(String name, int levelOfCompetence, DepartmentName departamentName) {
        this.name = name;
        this.levelOfCompetence = levelOfCompetence;
        this.departmentName = departamentName;
        this.status = CandidateStatus.AWAITING_RESPONSE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevelOfCompetence() {
        return levelOfCompetence;
    }

    public void setLevelOfCompetence(int levelOfCompetence) {
        this.levelOfCompetence = levelOfCompetence;
    }

    public DepartmentName getDepartamentName() {
        return departmentName;
    }

    public void setDepartmentName(DepartmentName departamentName) {
        this.departmentName = departamentName;
    }

    public CandidateStatus getStatus() {
        return status;
    }

    public void setStatus(CandidateStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return " Candidate " +
                "name '" + name + '\'' +
                ", level " + levelOfCompetence +
                ", departamentName " + departmentName +
                ", status " + status;
    }
}
