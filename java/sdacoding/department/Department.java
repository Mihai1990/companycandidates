package sdacoding.department;

import sdacoding.candidate.Candidate;
import sdacoding.candidate.CandidateStatus;

public class Department implements Evaluator {
    private int minimLevelOfCompetence;
    private DepartmentName departmentName;

    public Department() {
    }

    public Department(int minimLevelOfCompetence, DepartmentName departamentName) {
        this.minimLevelOfCompetence = minimLevelOfCompetence;
        this.departmentName = departamentName;
    }

    public void evaluate(Candidate candidate) {
        if (candidate.getLevelOfCompetence() <= minimLevelOfCompetence){
            candidate.setStatus(CandidateStatus.REJECTED);
        }else {
            candidate.setStatus(CandidateStatus.ACCEPTED);
        }

    }
}
