package sdacoding.department;

import sdacoding.candidate.Candidate;

public interface Evaluator  {
    void evaluate(Candidate candidate);
}
