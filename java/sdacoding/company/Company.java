package sdacoding.company;

import sdacoding.candidate.Candidate;
import sdacoding.department.Department;
import sdacoding.department.DepartmentName;
import sdacoding.department.Marketing;
import sdacoding.department.Production;
import sdacoding.exception.EvaluationIncapacityException;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private String companyName;
    List<Candidate> candidates = new ArrayList<Candidate>();
    List<Department> departaments = new ArrayList<Department>();

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public void recruiting() {

        Marketing marketing = new Marketing();
        Production production = new Production();
        for (Candidate c : candidates) {
            if (c.getDepartamentName().equals(DepartmentName.MARKETING)) {
                marketing.evaluate(c);
            } else if (c.getDepartamentName().equals(DepartmentName.PRODUCTION)) {
                production.evaluate(c);
            } else {
                throw new EvaluationIncapacityException();

            }

        }
    }
}
