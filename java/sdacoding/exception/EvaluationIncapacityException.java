package sdacoding.exception;

public class EvaluationIncapacityException extends RuntimeException {
    public EvaluationIncapacityException() {
        super("Candidatul nu poate fi evaluat");
    }
}
